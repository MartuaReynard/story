"""story URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include
from django.contrib import admin
from Story9 import views as v

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('homepage.urls')),
    path('kegiatan/', include('kegiatan.urls', namespace = 'templates')),
    path('Story7/', include(('Story7.urls','Story7'), namespace = 'Story7')),
    path('Story8/', include(('Story8.urls','Story8'), namespace = 'Story8')),
    path('login/', v.loginPage, name='login'),
    path('logout/', v.logoutUser, name='logout'),
    path('register/', v.register, name='register'),
    path('', include("django.contrib.auth.urls")),
]

