from django.urls import path
from .views import cari, search
from . import views

app_name = 'Story8'

urlpatterns = [
    path('', views.cari, name='cari'),
    path('buku/<str:key>', views.search, name='search'),
]
