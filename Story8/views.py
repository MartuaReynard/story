from django.shortcuts import render
from django.http import JsonResponse
import requests
import json

def cari(request):
    return render(request, 'cari.html')

def search(request, key):
    result= requests.get("https://www.googleapis.com/books/v1/volumes?q=" + key).json()
    return JsonResponse(result)
