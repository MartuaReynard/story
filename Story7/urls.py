from django.urls import path
from .views import index
from . import views


appname = 'Story7'

urlpatterns = [
   path('', views.index, name = 'accordion'),
]

