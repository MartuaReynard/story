from django.test import TestCase, Client
from . import views
from . apps import Story7Config

# # Create your tests here.

class Story7(TestCase):

    def test_page(self):
        response = Client().get("/Story7/")
        self.assertEqual(response.status_code,200)
    
    def testConfig(self):
        self.assertEqual(Story7Config.name, 'Story7')
    
    def test_kata_profil(self):
        response = Client().get('/Story7/')
        html = response.content.decode('utf-8')
        self.assertIn("Profil", html)
    
    def test_judul(self):
        response = Client().get('/Story7/')
        html = response.content.decode('utf-8')
        self.assertIn("Hallo! Saya Martua Reynard William Pasaribu", html)
