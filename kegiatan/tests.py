from django.test import TestCase, Client,RequestFactory
from django.urls import reverse, resolve
from .models import Kegiatan,Peserta
from . import views
from . import models
from .apps import KegiatanConfig


# Create your tests here.
class Tests(TestCase):
    def setUp(self):
        Kegiatan.objects.create(nama_kegiatan="hack ig orang")
        Peserta.objects.create(nama_peserta="rey",kegiatan=Kegiatan.objects.get(nama_kegiatan="hack ig orang"))

    def test_url_1(self):
        response = Client().get('/kegiatan/')
        self.assertEquals(response.status_code, 200)

    def test_url_2(self):
        response = Client().get('/kegiatan/tambahkegiatan')
        self.assertEquals(response.status_code, 200)
    
    def test_url_3(self):
        response = Client().get('/kegiatan/tambahpeserta')
        self.assertEquals(response.status_code, 200)
    
    def test_tombol_tambah(self):
        response = Client().get('/kegiatan/')
        html = response.content.decode('utf-8')
        self.assertIn("Tambah Peserta", html)
    
    def testConfig(self):
        self.assertEqual(KegiatanConfig.name, 'kegiatan')

    def test_tombol_tambahpeserta(self):
        response = Client().get('/kegiatan/tambahpeserta')
        html = response.content.decode('utf-8')
        self.assertIn("Tambah", html)
    
    def test_story6_save_a_POST_request(self):
        response = Client().post('/kegiatan/tambahkegiatan', data={'nama_kegiatan':'hack ig orang'})
        jumlah = models.Kegiatan.objects.filter(nama_kegiatan='hack ig orang').count()
        self.assertEqual(jumlah,2)
    
    def test_input_tombol_add3(self):
        response = Client().get('/kegiatan/tambahkegiatan')
        html = response.content.decode('utf-8')
        self.assertIn('Tambah', html)
