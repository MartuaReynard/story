from django.urls import path
from .views import Join, sched_delete, detail
from . import views

app_name = 'homepage'

urlpatterns = [
    path('', views.index, name='index'),
    path('biodata/', views.bio, name='bio'),
    path('studies/', views.studies, name='studies'),
    path('about/', views.about, name='about'),
    path('socmed/', views.socmed, name='socmed'),
    path('story1/', views.story1, name='story1'),
    path('Schedule/', views.Join, name='Schedule'),
    path('<int:pk>', sched_delete, name ='Delete'),
    path('detail/<int:pk>', views.detail, name ='Detail'),
]