from django.test import TestCase, Client,RequestFactory
from django.urls import reverse, resolve
from . import models
from . import views
from django.apps import apps
from . apps import HomepageConfig

class Tests(TestCase):
    def setUp(self):
        models.Schedule.objects.create(Matkul='a',Dosen='a',SKS='a',Deskripsi='a',Semester='a',Ruang='a')

    def testConfig(self):
        self.assertEqual(HomepageConfig.name, 'homepage')
    
    def test_apps(self):
        self.assertEqual(HomepageConfig.name, 'homepage') 
        self.assertEqual(apps.get_app_config('homepage').name, 'homepage')
    
    def test_url(self):
        response = Client().get('')
        self.assertEquals(200, response.status_code)
    
    def test_list_url_is_resolved(self):
        response = Client().get('/Schedule')
        self.assertEquals(response.status_code, 301)
    
    def test_list_url_is_resolved2(self):
        response = Client().get('/biodata/')
        self.assertEquals(response.status_code, 200)
    
    def test_list_url_is_resolved3(self):
        response = Client().get('/studies/')
        self.assertEquals(response.status_code, 200)
    
    def test_list_url_is_resolved4(self):
        response = Client().get('/about/')
        self.assertEquals(response.status_code, 200)
    
    def test_list_url_is_resolved5(self):
        response = Client().get('/socmed/')
        self.assertEquals(response.status_code, 200)
    
    def test_list_url_is_resolved6(self):
        response = Client().get('/story1/')
        self.assertEquals(response.status_code, 200)
    
    def test_form_save_a_POST_request(self):
        response = Client().post('/Schedule/', data={'Matkul':'a','Dosen':'a','SKS':'a','Deskripsi':'a','Semester':'a','Ruang':'a'})
        jumlah = models.Schedule.objects.filter(Matkul='a').count()
        self.assertEqual(jumlah,2)
    
    # def test_kasus_url_is_resolved3(self):
    #     response = Client().get('/Schedule/detail/1')
    #     self.assertEquals(response.status_code, 200)




