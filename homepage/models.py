from django.db import models

# Create your models here.
class Schedule(models.Model):
    Matkul = models.CharField(max_length=50)
    Dosen = models.CharField(max_length=50)
    SKS = models.CharField(max_length=50)
    Deskripsi = models.CharField(max_length=50)
    Semester = models.CharField(max_length=50) 
    Ruang = models.CharField(max_length=50)
    
    # Day = models.CharField(max_length=50)
    # Date = models.DateField(blank = False)
    # Time = models.TimeField()
    # Name = models.TextField(max_length=50)
    # Location = models.TextField(max_length=50)
    # Category = models.TextField(max_length=50)

