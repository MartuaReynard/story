from django.shortcuts import render, redirect
from .models import Schedule as jadwal
from .forms import SchedForm


# Create your views here.
def index(request):
    return render(request, 'index.html')

def bio(request):
    return render(request, 'bio.html')

def studies(request):
    return render(request, 'studies.html')

def about(request):
    return render(request, 'about.html')

def socmed(request):
    return render(request, 'socmed.html')

def story1(request):
    return render(request, 'story1.html')

def Join(request):
    form = SchedForm(request.POST)
    if request.method == "POST":
        if form.is_valid():
            sched = jadwal()
            sched.Matkul = form.cleaned_data['Matkul']
            sched.Dosen = form.cleaned_data['Dosen']
            sched.SKS = form.cleaned_data['SKS']
            sched.Deskripsi = form.cleaned_data['Deskripsi']
            sched.Semester = form.cleaned_data['Semester']
            sched.Ruang = form.cleaned_data['Ruang']
            # sched.Day = form.cleaned_data['Day']
            # sched.Date = form.cleaned_data['Date']
            # sched.Time = form.cleaned_data['Time']
            # sched.Name = form.cleaned_data['Name']
            # sched.Location = form.cleaned_data['Location']
            # sched.Category = form.cleaned_data['Category']
            sched.save()
            return redirect('/Schedule')
    
    sched = jadwal.objects.all()
    form = SchedForm()
    response = {"sched":sched, 'form' : form}
    return render(request,'Schedule.html',response)

def sched_delete(request, pk):
    form = SchedForm(request.POST)
    if request.method == "POST":
        if form.is_valid():
            sched = jadwal()
            sched.Matkul = form.cleaned_data['Matkul']
            sched.Dosen = form.cleaned_data['Dosen']
            sched.SKS = form.cleaned_data['SKS']
            sched.Deskripsi = form.cleaned_data['Deskripsi']
            sched.Semester = form.cleaned_data['Semester']
            sched.Ruang = form.cleaned_data['Ruang']
            # sched.Day = form.cleaned_data['Day']
            # sched.Date = form.cleaned_data['Date']
            # sched.Time = form.cleaned_data['Time']
            # sched.Name = form.cleaned_data['Name']
            # sched.Location = form.cleaned_data['Location']
            # sched.Category = form.cleaned_data['Category']
            sched.save()
            return redirect('/Schedule')
    
    jadwal.objects.filter(pk=pk).delete()
    data = jadwal.objects.all()
    form = SchedForm()
    response = {"sched":data, 'form' : form}
    return render(request, 'Schedule.html', response)

def detail(request, pk):
    sched = jadwal.objects.all().filter(id=pk)
    response = {"SchedForm" : sched}
    return render(request, "detail.html", response)
    