from django import forms

class SchedForm(forms.Form):

    Matkul = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Mata Kuliah',
        'type' : 'text',
        'required': True,
    }))

    Dosen = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nama Dosen',
        'type' : 'text',
        'required': True,
    }))

    SKS = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Jumlah SKS',
        'type' : 'text',
        'required': True,
    }))

    Deskripsi = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Deskripsi',
        'type' : 'text',
        'required': True,
    }))

    Semester = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Semester Tahun',
        'type' : 'text',
        'required': True,
    }))

    Ruang = forms.CharField(widget=forms.TextInput(attrs={
        'class': 'form-control',
        'placeholder': 'Nomor Ruang',
        'type' : 'text',
        'required': True,
    }))



    # Day = forms.CharField(widget=forms.TextInput(attrs={
    #     'class': 'form-control',
    #     'placeholder': 'Day',
    #     'type' : 'text',
    #     'required': True,
    # }))

    # Date = forms.DateField(widget=forms.DateInput(attrs={
    #     'class': 'form-control',
    #     'placeholder' : 'dd/mm/yyyy',
    #     'type' : 'date',
    #     'required': True,
    # }))

    # Time = forms.TimeField(widget=forms.TimeInput(attrs={
    #     'class': 'form-control',
    #     'type' : 'time',
    #     'required': True,
    # }))

    # Name = forms.CharField(widget=forms.TextInput(attrs={
    #     'class': 'form-control',
    #     'type' : 'text',
    #     'placeholder': 'Activity Name',
    #     'required': True,
    # }))

    # Location = forms.CharField(widget=forms.TextInput(attrs={
    #     'class': 'form-control',
    #     'type' : 'text',
    #     'placeholder': 'Activity Location',
    #     'required': True,
    # }))

    # Category = forms.CharField(widget=forms.TextInput(attrs={
    #     'class': 'form-control',
    #     'type' : 'text',
    #     'placeholder': 'Activity Type',
    #     'required': True,
    # }))
